//package edu.illinois.wala
//
//import scala.collection.JavaConversions.iterableAsScalaIterable
//import com.ibm.wala.types.ClassLoaderReference
//import com.typesafe.config.ConfigFactory
//import edu.illinois.wala.Facade.C
//import edu.illinois.wala.ipa.callgraph.AnalysisOptions
//import com.ibm.wala.classLoader.IBytecodeMethod
//import com.ibm.wala.ipa.callgraph.AnalysisCache
//import com.ibm.wala.ipa.slicer.NormalStatement
//import com.ibm.wala.ipa.callgraph.CallGraph
//import edu.illinois.wala.ipa.callgraph.FlexibleCallGraphBuilder
//import com.ibm.wala.ipa.callgraph.impl.Everywhere
//import com.ibm.wala.ipa.slicer.Slicer
//import com.ibm.wala.ipa.callgraph.impl.Util
//import com.ibm.wala.analysis.typeInference.TypeInference
//import edu.illinois.wala.ssa.V
//import com.ibm.wala.types.TypeReference
//import scala.collection.mutable.{HashMap, MultiMap}
//
//object Test {
//  def getOptions(): AnalysisOptions = {
//    implicit val config = ConfigFactory.load() // loads the above config file
//
//    val opts = AnalysisOptions()
//
//    opts
//  }
//
//  def main(args: Array[String]) {
//    implicit val config = ConfigFactory.load() // loads the above config file
//
//    val opts = AnalysisOptions()
//
//    import opts._
//
//    val cgb = Util.makeZeroCFABuilder(opts, new AnalysisCache(), cha, scope, null, null);
//
//    val pa = FlexibleCallGraphBuilder()
//
//    import pa._
//
//    def iS(cref: ClassLoaderReference): C => Boolean = {
//      x: C => x.getClassLoader.getReference equals cref
//    }
//
//    val klasses = cha filter iS(ClassLoaderReference.Application)
//
//    val methods = klasses flatMap { _.getDeclaredMethods } filter { _.isInstanceOf[IBytecodeMethod] }
//
//    val (inits, ms) = methods partition (_.isInit)
//
//    val mms = for (
//      m <- ms;
//      n = m.getNumberOfParameters;
//      r = m.getReturnType if !((r.getClassLoader equals ClassLoaderReference.Primordial)
//        || r.isPrimitiveType)
//    ) yield r -> ((0 until n) map { m getParameterType _ })
//
//    val minits = for (
//      m <- inits;
//      n = m.getNumberOfParameters
//    ) yield m.getDeclaringClass.getReference -> ((1 until n) map { m getParameterType _ })
//
//    val mapping = (minits ++ mms).toMap
//
//    val tests = (methods filter { _.getName.toString startsWith "test" }).toList
//    val test = tests(1)
//    val cache = new AnalysisCache
//    val ir = cache getIR test
//    val ti = TypeInference.make(ir, true)
//    val types = (1 until ir.getSymbolTable.getMaxValueNumber) map {ti getType _}
//    implicit val du = cache getDefUse ir
//    val ii = V(3).getDef
////    val node = cg getNode (test, Everywhere.EVERYWHERE)
////    val nodes = cg getNodes test.getReference
//
////    val ns = new NormalStatement(node, ir.getInstructions().size - 1)
////    val paa = cgb.getPointerAnalysis()
////    val slice = Slicer.computeBackwardSlice(ns, cg, paa)
//
//    println(types)
//  }
//}