package edu.udel.icdc.main

import java.io.File
import java.io.FileNotFoundException
import scala.Array.canBuildFrom
import scala.collection.JavaConversions.asScalaIterator
import scala.sys.process.stringSeqToProcess
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.types.ClassLoaderReference
import com.ibm.wala.util.scope.JUnitEntryPoints
import edu.illinois.wala.ipa.callgraph.AnalysisOptions
import edu.illinois.wala.ipa.callgraph.Dependency
import edu.udel.icdc.coverage.reader.ExecFileReader
import edu.udel.icdc.coverage.reader.ExecFileReader.CoverageBuilderWrapper
import edu.udel.icdc.coverage.reader.ValidMethodCoverageInfo
import edu.udel.icdc.main.Config.destFile
import edu.udel.icdc.main.Config.destFolder
import edu.udel.icdc.main.Config.jacocoAgentPath
import edu.udel.icdc.main.Config.mainClass
import edu.udel.icdc.main.Config.runnerCp
import edu.udel.icdc.main.Config.tracerSource
import edu.udel.icdc.main.Config.javaHome
import edu.udel.icdc.trace.TraceRecord
import edu.udel.icdc.utils.ImplicitConversions.SubClassWrapper
import edu.udel.icdc.utils.ImplicitConversions.ref2i
import edu.udel.icdc.utils.ImplicitConversions.i2ref
import edu.udel.icdc.utils.Types.JUnitTest
import edu.udel.icdc.utils.Types.SingleTestCoverageInfo
import edu.udel.icdc.utils.Types.TestSuiteCoverageInfo
import edu.udel.icdc.utils.Types.appCL
import edu.udel.icdc.utils.Utils
import edu.udel.icdc.utils.Utils.iS
import edu.udel.icdc.utils.Types
import edu.udel.icdc.utils.Types.ShallowMap
import edu.udel.icdc.trace.TraceRecord
import com.ibm.wala.types.MethodReference
import com.ibm.wala.classLoader.IClass

import edu.udel.icdc.utils.ImplicitConversions.TupleListWrapper

case class SubjectRunner(val subject: String, val opts: AnalysisOptions, val cha: ClassHierarchy) {
	implicit val ch = cha

	def createIfNotExist(p: String) = {
		val fp = new File(p)
		if (!fp.exists()) fp.mkdirs()
	}

	def profileOneSubject = {

		import opts.dependencies
		implicit val ch = cha

		val tests = Utils.getTestMethods(subject) //filter (tofilter contains _)
		println(tests.length)

		val cocoCommand = genCocoCommand(dependencies, _: MethodReference, subject)
		val compileTracerCommand = genCompileTracerCommand(_: MethodReference)
		val tracerCommand = genTracerCommand(dependencies, _: MethodReference, subject)

		val cocoFolder = destFolder(subject, "coco")
		val traceFolder = destFolder(subject, "trace")

		createIfNotExist(cocoFolder)
		createIfNotExist(traceFolder)

		//		{
		//			Seq("rm", "-rf", cocoFolder).lines
		//			Seq("mkdir", cocoFolder).lines
		//			Seq("rm", "-rf", traceFolder).lines
		//			Seq("mkdir", traceFolder).lines
		//		}

		//		cleanCommand.lines

		//		val commands = tests map { t => cocoCommand(t) }

		val ret = tests map { t =>
			{
				try {
					println(MethodRef2Args(t))
					cocoCommand(t).lines
					compileTracerCommand(t).lines
					tracerCommand(t).lines
				} catch {
					case e: RuntimeException =>
						Stream[String]("re"); println(e)
					case e: FileNotFoundException => Stream[String]("file not found"); println(e)
				}
			}
		}
	}

	def readOneSubject: TestSuiteCoverageInfo = {
		import opts.clsRoot
		implicit val loaderRef = ClassLoaderReference.Application
		implicit val ch = cha

		val execFiles = new File(destFolder(subject, "coco")) listFiles

		execFiles map { f => readOneExecFile(f, clsRoot) } toMap
		//		println(ret groupBy {case (t, ms) => t} filter {case (t, m) => m.size > 1} map {case (t, ms) => ms.toMap})
		//		println(ret.toMap map {case (t, ms) => t} groupBy {t => t} filter {tu => tu._2.size > 1})
	}

	def readTraceFiles: ShallowMap = {
		import opts.clsRoot
		implicit val loaderRef = ClassLoaderReference.Application
		implicit val ch = cha

		val traceFiles = new File(destFolder(subject, "trace")) listFiles

		traceFiles map { f => readOneTraceFile(f, clsRoot) } toMap
	}

	def readOneTraceFile(traceFile: File, clsRoot: String) = {
		val fname = traceFile.getName
				println("tracer: " + fname)
		val r = scala.io.Source.fromFile(traceFile)
		val iter = r.getLines

		var lst = List[MethodReference]()

		try {
			while (iter.hasNext) {
				val s = iter.next
				if ((s split "\t" length) == 6) {

					val ss = s split "\t"
					val callee = Types.trace2IMethod(ss(0) dropRight 1, ss(1), ss(2), cha)
					val caller = Types.trace2IMethod(ss(3) dropRight 1, ss(4), ss(5), cha)
					if (callee != null && caller != null) {
						val tr = TraceRecord(callee, caller)(cha)
						if (tr.isShallow)
							lst = tr.callee :: lst
					}
				}
			}
		} catch {
			case e: java.nio.charset.MalformedInputException => println(traceFile.getAbsolutePath)
		}

		r.close

		val method = fname split "\\." last
		val cls = fname split "\\." dropRight 1 mkString "."

		val testMethod = Types.trace2IMethod(cls, method, "()V", cha)

		testMethod -> lst
	}

	def readOneExecFile(execFile: File, clsRoot: String) = {
		val fname = execFile.getName
				println("coco: " + fname)
		val classes = Utils.getIClasses(cha) filterNot { cls => cls.getName isSubOf JUnitTest }
		val appClasses = classes filter iS(appCL)

		//get coverage builder for every class from one single exec file
		val cbs = appClasses map { (cls => (ExecFileReader(new File(clsRoot), execFile)).getCoverageBuilder(cls.getName.toString.drop(1))) }

		val infos = cbs flatMap { cb => cb.parseBuilder } flatMap { cinfo =>
			cinfo.collectMethodCoverageInfos
		} collect { case ValidMethodCoverageInfo(key, coveredLines) => key.getReference -> coveredLines }

		val method = fname split "\\." last
		val cls = fname split "\\." dropRight 1 mkString "."

		val testMethod = Types.trace2IMethod(cls, method, "()V", cha)
		//println(testMethod, infos)

		(testMethod, infos.toMap)
	}

	private def genCocoCommand(deps: Seq[Dependency], m: MethodReference, subject: String) = {
		val cp = (deps map { _.file }) ++ List(runnerCp) mkString ":"
		val agent = "-javaagent:" + jacocoAgentPath + "=destfile=" + destFile(subject, "coco", m)

		//				println(Seq("java", "-cp", cp, agent, mainClass, MethodRef2Args(m)) mkString " ")
		Seq("timeout", "60s", "java", "-cp", cp, agent, mainClass, MethodRef2Args(m))
	}

	private def genTracerCommand(deps: Seq[Dependency], m: MethodReference, subject: String) = {
		val cp = (deps map { _.file }) ++ List(runnerCp) mkString ":"
		val tracer = "-agentpath:" + Config.tracerPath + "=" + destFile(subject, "trace", m)

		//			println(Seq("java", "-cp", cp, tracer, mainClass, MethodRef2Args(m)) mkString " ")
		Seq("timeout", "60s", "java", "-cp", cp, tracer, mainClass, MethodRef2Args(m))
	}

	private def genCompileTracerCommand(m: MethodReference) = {
		//val command = "gcc -fPIC -DTEST=test1 -shared -o tracer.so -I${JAVA_HOME}/../include -I${JAVA_HOME}/../include/linux tracer.c"
		//		println(Seq("gcc", "-fPIC", "-DTEST="+m.getDeclaringClass.getName.toString.split("/").last, "-shared", "-o", "tracer.so", "-I"+javaHome+"/../include", "-I"+javaHome+"/../include/linux", tracerSource) mkString " ")
		Seq("gcc", "-fPIC", "-DTEST=" + m.getDeclaringClass.getName.toString.split("/").last, "-shared", "-o", Config.tracerPath, "-I" + javaHome + "/../include", "-I" + javaHome + "/../include/linux", tracerSource)
	}

	private def MethodRef2Args(m: MethodReference) = {
		val cls = m.getDeclaringClass.getName.toString.replace('/', '.').drop(1)
		val method = m.getName.toString
		cls + "#" + method
	}
}

object SubjectRunner {

	def apply(subject: String) = {
		val opts = Utils.getOptions(subject + ".conf")

		new SubjectRunner(subject, opts, opts.cha)
	}
}
