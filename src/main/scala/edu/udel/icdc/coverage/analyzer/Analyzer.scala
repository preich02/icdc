package edu.udel.icdc.coverage.analyzer

import edu.udel.icdc.utils.Types._
import com.ibm.wala.classLoader.IMethod
import scala.collection.mutable.PriorityQueue
import scala.collection.mutable.Map
import edu.udel.icdc.utils.ImplicitConversions.MapWrapper
import java.io.File
import java.io.FileWriter
import scala.util.Random

case class TestProfile(test: IMethod, ratio: Double, ds: Map[IMethod, Set[Int]], 
		is: Map[IMethod, Set[Int]], cmp: Ordering[TestProfile]) extends Ordered[TestProfile] {

	def dSize = ds.cSize

	override def compare(that: TestProfile) = cmp.compare(this, that)
//		if (ratio == 0.0)
//			dSize.compareTo(that.dSize)
//		else
//			ratio.compareTo(that.ratio)

	override def toString = test.getName.toString + "\t" + ratio //+ "\n\t" + ds + "\n\t" + is + "\n\n"
	def toFile = {
		val cName = test.getDeclaringClass.getName.toString split "/"
		val c = cName mkString "." drop 1

		(c, test.getName.toString)
//		pName + "\t" + klass + "\t" + test.getName.toString
	}
}

case class RandomAnalyzer(total: Int, var profileQueue: PriorityQueue[TestProfile], cmp: Ordering[TestProfile]) 
		extends Analyzer {
	val random = new Random

	def getR(a: Int, b: Int) = random.nextDouble()
}

case class StrictAnalyzer(total: Int, var profileQueue: PriorityQueue[TestProfile],
				cmp: Ordering[TestProfile]) extends Analyzer {
	def getR(sizeD: Int, sizeI: Int) = 
		if (sizeD == 0 && sizeI == 0) 0.0 else sizeI.toDouble / (sizeI + sizeD)
}

abstract class Analyzer {
	def total: Int
	var profileQueue: PriorityQueue[TestProfile]
	def cmp: Ordering[TestProfile]

	val addedD = Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)
	val addedI = Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)
	val mPool = Map[String, Set[String]]().withDefaultValue(Set.empty)
	var totalIRatio = 0.0
	var originQueue = profileQueue


	def getR(a: Int, b: Int): Double

	protected def updateRatio(prev: TestProfile)(tp: TestProfile) = {
		prev.ds foreach {
			case (k, v) => {
				tp.is.update(k, tp.is(k) diff v)
				tp.ds.update(k, v union tp.ds(k))
			}
		}

		val sizeD = tp.ds.cSize
		val sizeI = tp.is.cSize

		val r = getR(sizeD, sizeI)
		//		println(sizeD, sizeI, sizeI.toDouble / (sizeI + sizeD))
		//		if (sizeD==0 && sizeI==0)
		//			EmptyProfile
		//		else
		new TestProfile(tp.test, r, tp.ds, tp.is, cmp)
	}

	protected def updatedAdded(tp: TestProfile) = {

		tp.ds foreach {
			case (k, v) =>
				addedD.update(k, addedD(k) union v)
				addedI.update(k, addedI(k) diff v)
		}

		tp.is foreach {
			case (k, v) => {
				val ic = v diff addedD(k)
				addedI.update(k, addedI(k) union ic)
			}
		}

		val totI = addedI.cSize
		val totD = addedD.cSize
		totalIRatio = totI.toDouble / (totI + totD)
	}

	def dequeue = {
		val popped = profileQueue dequeue ()
		updatedAdded(popped)
		val f = updateRatio(popped)(_)
		profileQueue = profileQueue map { x => f(x) }
	}

	def saturation = (addedI.cSize.toDouble + addedD.cSize) / total

	def cInfo = (addedD, addedI)
	
	def logTheRest(log: String) = {
		val re = this.report

		while (profileQueue.nonEmpty) {
			val p = profileQueue dequeue ()
			val (k,v) = p.toFile
			mPool.update(k, mPool(k) + v)
		}
		
		val logFile = new File(log)
		if (!logFile.getParentFile().exists()) {
			logFile.getParentFile().mkdirs()
		}

		val fw = new FileWriter(logFile)
		fw.write(re+"\n")
		fw.write(mPool map {case (k,v) => k+":"+(v mkString "\t")} mkString "\n")
		fw.close
	}

	def report = {
		val totI = addedI.cSize
		val totD = addedD.cSize
		"directly:\t" + totD + "\tindirectly:\t" + totI + "\tratio\t" + (1 - totalIRatio) +
			"\tcoverage\t" + saturation
	}
}

object Analyzer {

	def generateSkippingTests(ana: Analyzer, log: String, sat: Double, cnt: Int) = {
		while (ana.saturation < sat) {
			ana.dequeue
		}

		println(ana.report)
		ana.logTheRest(log++cnt.toString)
	}

	val maxHeaping = new Ordering[TestProfile]() {
		//max expects lower mutation score
		override def compare(dis: TestProfile, that: TestProfile) =
			if (dis.ratio == 0.0)
				dis.dSize.compareTo(that.dSize)
			else
				dis.ratio.compareTo(that.ratio)
	}

	val minHeaping = new Ordering[TestProfile]() {
		override def compare(dis: TestProfile, that: TestProfile) =
			if (dis.ratio == 0.0)
				that.dSize.compareTo(dis.dSize)
			else
				that.ratio.compareTo(dis.ratio)
	}

	def apply(directs: ShallowMap, db: TestSuiteCoverageInfo, cmp: Ordering[TestProfile], random: Boolean) = {
		val all = Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)

		db foreach {
			case (t, v) =>
				v foreach {
					case (k, vv) => all.update(k, all(k) union vv)
				}
		}

		val totLines = all.cSize
		//_1 direct, _2 indirect
		val splitted = db map { case (t, md) => t -> (md partition { case (m, s) => directs.getOrElse(t, List()).toList contains m }) }

		def getLocalRatio(ds: SingleTestCoverageInfo, is: SingleTestCoverageInfo) = {
			val sizeD = ds.cSize
			val sizeI = is.cSize

			sizeI.toDouble / (sizeI + sizeD)
		}

		val priQueue = new PriorityQueue[TestProfile]()

		splitted foreach {
			case (t, (lds, lis)) =>
				priQueue enqueue new TestProfile(t, getLocalRatio(lds, lis),
					(Map() ++= lds).withDefaultValue(Set.empty), (Map() ++= lis).withDefaultValue(Set.empty), cmp)
		}
		println(splitted)

		if (random)
			new RandomAnalyzer(totLines, priQueue, cmp)
		else
			new StrictAnalyzer(totLines, priQueue, cmp)
	}
}
