package edu.udel.icdc.coverage.analyzer

import com.ibm.wala.classLoader.IBytecodeMethod
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.callgraph.AnalysisCache
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.shrikeCT.InvalidClassFileException

sealed trait MethodLineInfo

case class ValidMethodLineInfo(val key: IMethod, val firstLine: Option[Int], val lastLine: Option[Int]) extends MethodLineInfo {
	def isComplete = firstLine.nonEmpty && lastLine.nonEmpty
}

object EmptyMethodLineInfo extends MethodLineInfo

object MethodLineInfo {

	def apply(m: IBytecodeMethod)(implicit cha: ClassHierarchy, cache: AnalysisCache) = {
		implicit val mm = m
		val ir = cache.getIR(m)
		if (ir == null) EmptyMethodLineInfo
		else {
			val insns = ir.getInstructions
			
			val lines = Range(0, insns.length) map getLineNumber collect {case Some(i) => i} distinct
			
			if (lines.nonEmpty) 
				new ValidMethodLineInfo(m, Some(lines.min), Some(lines.max))
			else 
				new ValidMethodLineInfo(m, None, None)
		}
	}

	def getLineNumber(index: Int)(implicit m: IBytecodeMethod) =
		getBCO(index) flatMap { bco => Some(m getLineNumber bco) }

	def getBCO(index: Int)(implicit m: IBytecodeMethod) =
		if (index == -1) None
		else {
			try {
				Some(m getBytecodeIndex index)
			} catch {
				case e: InvalidClassFileException => None
			}
		}

}