package edu.udel.icdc.coverage.analyzer

import edu.udel.icdc.utils.Types._
import edu.udel.icdc.utils.ImplicitConversions.SubClassWrapper
import edu.udel.icdc.utils.ImplicitConversions.SubClassWrapperForMethods
import edu.udel.icdc.utils.ImplicitConversions.MapWrapper
import edu.udel.icdc.utils.ImplicitConversions.ref2i
import edu.udel.icdc.utils.ImplicitConversions.i2ref
import edu.udel.icdc.coverage.reader.BugInfo
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.types.MethodReference
import java.io.FileWriter
import java.io.File
import edu.udel.icdc.main.Config

case class BasicTestProfile(test: MethodReference, ds: Map[MethodReference, Set[Int]], is: Map[MethodReference, Set[Int]])

case class BasicAnalyzer(profiles: List[BasicTestProfile]) {

	val dPool = collection.mutable.Map[MethodReference, Set[Int]]().withDefaultValue(Set.empty)
	val iPool = collection.mutable.Map[MethodReference, Set[Int]]().withDefaultValue(Set.empty)

	def update(dc: Map[MethodReference, Set[Int]], ic: Map[MethodReference, Set[Int]]) = {
		dc foreach {
			case (m, s) => {
				dPool.update(m, dPool(m) union s)
				iPool.update(m, iPool(m) diff s)
			}
		}

		ic foreach {
			case (m, s) => iPool.update(m, iPool(m) union (s diff dPool(m)))
		}
	}

	def analyze = {
		profiles foreach {
			case p @ BasicTestProfile(test, dc, ic) => update(dc, ic)
		}

	}

	def getDC = dPool
	def getIC = iPool

	def overloading(sub: String)(implicit cha: ClassHierarchy) = {
		val f = new FileWriter(new File(Config.projectRoot + "/data/study/" + sub + ".over"))

		def score(m: MethodReference) = {
			dPool(m).size.toFloat / (dPool(m).size.toFloat + iPool(m).size.toFloat)
		}

		f.write(
			(iPool.keys ++ dPool.keys).toSet groupBy { m: MethodReference => m.getSignature.split("\\(")(0) } filterNot {
				case (k, v) => v.size == 1
			} map { case (k, v) => v } map { l => (l map { m => (m.getSignature, score(m)) }) mkString "\n" } mkString "\n\n")

		f.close()
	}

	def inheritance(sub: String)(implicit cha: ClassHierarchy) = {
		val f = new FileWriter(new File(Config.projectRoot + "/data/study/" + sub + ".in"))
		def score(m: MethodReference) = {
			dPool(m).size.toFloat / (dPool(m).size.toFloat + iPool(m).size.toFloat)
		}

		def groupByFamily(lst: Iterable[MethodReference]) = {
			val map = collection.mutable.Map[MethodReference, Set[MethodReference]]()
			lst foreach { m => map update (m, Set.empty) }
			for (m <- lst) {
				map.keys foreach {
					k =>
						if (k == m) {}
						else if (k isSubOf m) {
							map update (m, map(k) + k)
							map remove k
						} else if (m isSubOf k) {
							map update (k, map(k) + m)
							map remove m
						}
				}
			}

			//			map foreach { case (k, v) => if (k.toString contains "findOption") println(k, v)}
			map filter { case (k, v) => v.nonEmpty } map { case (k, v) => k :: v.toList }
		}

		//		(iPool.keys.toSet ++ dPool.keys.toSet) foreach {m : MethodReference => if (m.toString contains "findOption") println(m)}

		f.write(
			(iPool.keys ++ dPool.keys).toSet groupBy { m: MethodReference => m.getSelector } map {
				case (k, v) => v
			} flatMap {
				groupByFamily(_)
			} map { l => l map { m => (m.getSignature, score(m)) } mkString "\n" } mkString "\n\n")
		//			} map { l => (l map { m => (m, score(m)) }) mkString "\n" } mkString "\n\n")

		f.close()
	}

	def writeRatio(sub: String) = {
		val f = new FileWriter(new File(Config.projectRoot + "/writing/current/data/" + sub + ".csv"))
		f.write("method,dc\n")
		dPool foreach {
			case (m, s) =>
				f.write(m.getSignature() + "," +
					(s.size.toFloat / (s.size + iPool(m).size)) + "\n")
		}

		iPool foreach {
			case (m, s) =>
				if (dPool(m).isEmpty) {
					//					if ((m.toString contains "AlreadySelectedException"))
					//						println(m.getSignature, s)
					f.write(m.getSignature() + ",0.0\n")
				}
		}

		f.close()
	}

	def study(sub: String) = {
		val f = new FileWriter(new File(Config.projectRoot + "/data/study/" + sub))

		def searchProfiles(m: MethodReference) = {
			profiles map {
				case p @ BasicTestProfile(test, ds, is) =>
					if (ds(m).isEmpty && is(m).nonEmpty) (test.getSignature + "\t" + is(m)) else ""
				//					if (ds(m).nonEmpty && is(m).nonEmpty) (test.getSignature + ds(m) + "\t" + is(m)) else ""
			} filterNot { _ == "" } mkString "\n\t"
		}

		iPool foreach {
			case (m, s) => if (dPool(m).isEmpty)
				//			case (m, s) => if (dPool(m).nonEmpty)
				f.write(m.getSignature + "\nic: " + iPool(m) + dPool(m) + "\n\t" + searchProfiles(m) + "\n")
		}

		f.close()
	}

}

object BasicAnalyzer {

	def apply(directs: ShallowMap, db: TestSuiteCoverageInfo)(implicit cha: ClassHierarchy) = {
		val all = scala.collection.mutable.Map[MethodReference, Set[Int]]().withDefaultValue(Set.empty)

		db foreach { case (t, v) => v foreach { case (k, vv) => all.update(k, all(k) union vv) } }

		println("all", all.size)
		println(directs.size)

		//_1 direct, _2 indirect
		val splitted = db map {
			case (t, md) => t -> (
				md partition {
					case (m, s) => directs.getOrElse(t, List()).toList contains m
				})
		}

		val profiles = splitted map {
			case (t, (lds, lis)) => {
				//				new BasicTestProfile(t, lds.withDefaultValue(Set.empty), (lis filter {case (m, _)=> m.isPrivate}).withDefaultValue(Set.empty))

				new BasicTestProfile(t, lds.withDefaultValue(Set.empty), (lis filter {
					case (m, _) => m.isPublic &&
						cha.resolveMethod(m).getDeclaringClass.isPublic
				}).withDefaultValue(Set.empty))
			}
		}

		new BasicAnalyzer(profiles.toList)
	}
}
