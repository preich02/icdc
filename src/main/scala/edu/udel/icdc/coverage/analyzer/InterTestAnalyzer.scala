package edu.udel.icdc.coverage.analyzer

import edu.udel.icdc.utils.Types._
import com.ibm.wala.classLoader.IMethod
import edu.illinois.wala.Facade._
import edu.udel.icdc.utils.ImplicitConversions._

case class TestProfile(val test: IMethod, val directs: SingleTestCoverageInfo, val indirects: SingleTestCoverageInfo) {
	override def toString = "\n<" + test.name + " :: \n" + "directs :" +
		(directs map { e => e._1.name + e._2.toString }) + ">\n"
}

case class Record(val name: IMethod, val icovered: SingleTestCoverageInfo, val dcovered: SingleTestCoverageInfo) {
	override def toString = "\n" + name.toString + " ->\n" + "indirectly:\n" + 
		(icovered map {
			case (m, s) => "\n\t" + m.getSignature + " : " + s.toList.sorted.toString
		}) +  "\nDirectly\n" +
		(dcovered map {
			case (m, s) => "\n\t" + m.getSignature + " : " + s.toList.sorted.toString
		})
}

case class ProfileStore(val db: SolvedTestSuiteCoverageInfo) {

	private def m2key(m: IMethod) =
		m.getDeclaringClass.getName.toString.replace("/", ".") + "." + m.getName.toString

	def analyze = {
		val directpool = (Set.empty[IMethod] /: db) ((acc, md) => acc union md._2._1.keys.toSet[IMethod])

		db map { case(t, (ds, is)) => t -> (ds, unionOfDirects(ds, is))}
	}
}

object ProfileStore {

	private def m2key(m: IMethod) =
		m.getDeclaringClass.getName.toString.replace("/", ".") + "." + m.getName.toString
}