package edu.udel.icdc.coverage.analyzer

import com.ibm.wala.classLoader.IMethod

case class InversedAnalyzer(val occurence: Map[String, Seq[IMethod]]) {
	override def toString = occurence map {case (k,v) => k + (v mkString "\t\n")} mkString "\n\n"
//	def top10 = (occurence.toList.sortBy { case (k, v) => v } reverse) take 10
}

object InversedAnalyzer {
	def apply(records: Iterable[Record]) = {
		val occ = records.toList flatMap {
			case Record(n, ic, dc) =>
				ic.keys.toList
		} groupBy identity map {
			case (k, v) =>
				k.getSignature -> v.toSeq
		}

		new InversedAnalyzer(occ)
	}
}