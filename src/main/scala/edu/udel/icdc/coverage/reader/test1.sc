package edu.udel.coco.reader

import java.io.File
import edu.udel.coco.utils.ImplicitConversions._

import com.typesafe.config.ConfigFactory

import edu.udel.coco.utils.Utils
import edu.udel.coco.reader.Reader._

object test1 {
	val projectHome = "/home/huoc/Projects/new_project/tool/"
                                                  //> projectHome  : String = /home/huoc/Projects/new_project/tool/

	val projectName = "/CocoReader/"          //> projectName  : String = /CocoReader/
	val configName = "Sudoku.conf"            //> configName  : String = Sudoku.conf
	val subject = configName takeWhile { _ != '.' }
                                                  //> subject  : String = Sudoku

	val file = new File(projectHome + projectName + configName)
                                                  //> file  : java.io.File = /home/huoc/Projects/new_project/tool/CocoReader/Sudok
                                                  //| u.conf
	val confFile = ConfigFactory.parseFile(file)
                                                  //> confFile  : com.typesafe.config.Config = Config(SimpleConfigObject({"wala":{
                                                  //| "jre-lib-path":"/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar","dependenc
                                                  //| ies":{"binary":${?wala.dependencies.binary}["/home/huoc/research/facade/subj
                                                  //| ects//Sudoku/build/test/original"],"binary":${?wala.dependencies.binary}["/h
                                                  //| ome/huoc/research/facade/subjects/Sudoku/build/app"],"jar":${?wala.dependenc
                                                  //| ies.jar}["/home/huoc/Projects/testassist/subjects/lib/junit-4.11.jar"],"jar"
                                                  //| :${?wala.dependencies.jar}["/home/huoc/Projects/testassist/subjects/lib/hamc
                                                  //| rest-all-1.3.jar"]},"entry":{"class":"Lsudoku/TestSudoku","method":"testInit
                                                  //| ialize_Empty()V"},"exclussions":""}}))

	implicit val conf = ConfigFactory.load(confFile)
                                                  //> conf  : com.typesafe.config.Config = Config(SimpleConfigObject({"os":{"arch"
                                                  //| :"amd64","name":"Linux","version":"3.2.0-23-generic"},"awt":{"toolkit":"sun.
                                                  //| awt.X11.XToolkit"},"file":{"encoding":{"pkg":"sun.io"},"separator":"/"},"pat
                                                  //| h":{"separator":":"},"line":{"separator":"\n"},"java":{"vm":{"vendor":"Oracl
                                                  //| e Corporation","name":"OpenJDK 64-Bit Server VM","specification":{"vendor":"
                                                  //| Oracle Corporation","name":"Java Virtual Machine Specification","version":"1
                                                  //| .7"},"version":"24.65-b04","info":"mixed mode"},"home":"/usr/lib/jvm/java-7-
                                                  //| openjdk-amd64/jre","awt":{"graphicsenv":"sun.awt.X11GraphicsEnvironment","pr
                                                  //| interjob":"sun.print.PSPrinterJob"},"vendor":{"url":{"bug":"http://bugreport
                                                  //| .sun.com/bugreport/"}},"endorsed":{"dirs":"/usr/lib/jvm/java-7-openjdk-amd64
                                                  //| /jre/lib/endorsed"},"library":{"path":"/usr/java/packages/lib/amd64:/usr/lib
                                                  //| /x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/l
                                                  //| ib/jni:/lib:/usr/lib"},"
                                                  //| Output exceeds cutoff limit.

	implicit val opts = Utils.getOptions()    //> opts  : edu.illinois.wala.ipa.callgraph.AnalysisOptions = edu.illinois.wala.
                                                  //| ipa.callgraph.AnalysisOptions@35988e16

	import opts._

	//		implicit val ch = cha

	val cInfos = Reader(
		new File("/home/huoc/Projects/testassist/subjects/Sudoku/build/app/"),
		new File("/home/huoc/Desktop/"))
		.getCoverageBuilder("sudoku/Puzzle").parseBuilder
                                                  //> cInfos  : List[edu.udel.coco.reader.CoverageInfo] = List(CoverageInfo(Lsudok
                                                  //| u/Puzzle,List(9, 15, 16, 17, 19, 21, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32,
                                                  //|  35, 48, 49, 50, 51, 54, 70, 71, 72, 74, 75, 78, 80, 81, 82, 97, 98, 99, 100
                                                  //| , 101, 102, 104, 108, 109, 110, 111, 112, 115, 116, 117, 118, 120, 123, 124,
                                                  //|  125, 126, 128, 129, 130, 131, 132, 136, 140, 141, 142, 143, 144, 145, 148, 
                                                  //| 152, 156, 160, 161, 162, 165, 169, 170, 174, 175, 176, 177, 178, 181, 185, 1
                                                  //| 89, 193, 197, 201, 205, 206, 207, 208, 209, 213, 214, 215, 216, 217, 220, 22
                                                  //| 4, 225, 226, 230, 234, 235, 237, 238, 239, 242, 243, 244, 245, 246, 250, 251
                                                  //| , 252, 253, 254, 255, 256, 257, 258, 260, 261, 262, 265, 269, 270, 271, 272,
                                                  //|  275, 276, 277, 278)))

	println(cInfos map { _.collectMethodCoverageInfos(cha) })
                                                  //> Lsudoku/Puzzle
                                                  //| List(List(<init> : Set(21, 17, 22, 16, 19, 15), <init> : Set(25, 29, 28, 27,
                                                  //|  31, 26, 30), solve : Set(), solve : Set(48, 49, 50, 51), propagateConstrain
                                                  //| ts : Set(78, 74, 71, 81, 80, 72, 75), search : Set(101, 102, 97, 98, 99, 100
                                                  //| ), checkForDuplicates : Set(108, 109, 110, 111), removeSolvedValuesFromPeers
                                                  //|  : Set(115, 116, 117, 118), getUniquelySpecifiedBoxes : Set(125, 132, 124, 1
                                                  //| 29, 128, 130, 123, 126, 131), getUnsolvedBoxes : Set(142, 141, 144, 140, 145
                                                  //| , 143), dimension : Set(), getSolvedBoxes : Set(), getSolvedValue : Set(160,
                                                  //|  161, 162), getPossibleValues : Set(169), isSolved : Set(174, 176, 177, 175,
                                                  //|  178), getPeers : Set(), getPeersInSameRow : Set(), getPeersInSameColumn : S
                                                  //| et(), getPeersInSameSubSquare : Set(), getBox : Set(), solutionAsSingleStrin
                                                  //| g : Set(205, 206, 207, 208), min : Set(217, 216, 213, 214, 215), doesPossibl
                                                  //| eValueAppearInAnyPeers : Set(224, 225, 226), validateInputString : Set(234, 
                                                  //| 235, 
                                                  //| Output exceeds cutoff limit.
}