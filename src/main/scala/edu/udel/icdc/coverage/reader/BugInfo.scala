package edu.udel.icdc.coverage.reader

import com.ibm.wala.classLoader.IMethod
import java.io.FileReader
import java.io.File
import com.ibm.wala.types.MethodReference
import com.ibm.wala.util.strings.Atom
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.types.Descriptor
import com.ibm.wala.types.TypeReference
import com.ibm.wala.classLoader.Language
import edu.udel.icdc.utils.Types._

case class BugInfo(tests: List[IMethod], bugs: Map[IMethod, Set[Int]]) {
}

object BugInfo {

	def str2IMethod(s: String)(implicit cha: ClassHierarchy) = {
		val cls = s.split("::")(0)
		val m = s.split("::")(1)

		val klass = TypeReference.findOrCreate(appCL, "L"+cls.replace('.', '/'))
		val atom = Atom.findOrCreateUnicodeAtom(m)
		val desc = Descriptor.findOrCreateUTF8(Language.JAVA, "()V")
		val ref = MethodReference.findOrCreate(klass, atom, desc)
		cha.resolveMethod(ref)
	}
	
	def readBug(s: String)(implicit cha: ClassHierarchy) = {
		val rawClass = s.split("', ")(0)
		val rawLines = s.split("', ")(1)
		val cls = rawClass drop 2
		val lines = (((rawLines dropRight 2) drop 1) split ", ").toList map (_.toInt)
		val clsName = "L"+cls.replace('.', '/')
//		val cov = new CoverageInfo(clsName, lines)
		var iter = cha.iterator()
		var ss = scala.collection.mutable.Set[CoverageInfo]()
		while (iter.hasNext()) {
			var ic = iter.next()
			if (ic.toString() contains clsName) {
				ss.add(new CoverageInfo(ic.getName.toString, lines))
			}
		}
		ss flatMap (_.collectMethodCoverageInfos collect { 
			case ValidMethodCoverageInfo(key, coveredLines) => key -> coveredLines })
	}

	def apply(path: String)(implicit cha: ClassHierarchy) = {
		val testsFile = io.Source.fromFile(path+"/tests") 
		val lines = testsFile.getLines.toList
		testsFile.close()
		val tests = lines map str2IMethod

		val bugFile = io.Source.fromFile(path+"/lines") 
		val flines = bugFile.getLines.toList
		bugFile.close()
		val bugs = flines flatMap readBug
		
		new BugInfo(tests, bugs.toMap)
	}
}