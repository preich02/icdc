package edu.udel.icdc.coverage.reader

import java.io.File

import java.io.FileInputStream
import scala.collection.JavaConversions.iterableAsScalaIterable
import org.jacoco.core.analysis.Analyzer
import org.jacoco.core.analysis.CoverageBuilder
import org.jacoco.core.data.ExecutionData
import org.jacoco.core.data.ExecutionDataReader
import org.jacoco.core.data.ExecutionDataStore
import org.jacoco.core.data.IExecutionDataVisitor
import org.jacoco.core.data.ISessionInfoVisitor
import org.jacoco.core.data.SessionInfo
import com.ibm.wala.classLoader.IBytecodeMethod
import com.ibm.wala.classLoader.IClass
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.callgraph.AnalysisCache
import com.ibm.wala.ipa.cha.ClassHierarchy
import edu.udel.icdc.coverage.analyzer.MethodLineInfo
import org.jacoco.core.analysis.ICounter
import com.ibm.wala.types.TypeName
import com.ibm.wala.types.TypeReference
import edu.udel.icdc.utils.Types
import com.ibm.wala.classLoader.ShrikeBTMethod
import edu.udel.icdc.coverage.analyzer.ValidMethodLineInfo
import java.io.FileNotFoundException
import edu.udel.icdc.utils.Utils.iS
import edu.udel.icdc.utils.Types.appCL

sealed trait MethodCoverageInfo {
	override def toString = "haha"
}

case class ValidMethodCoverageInfo(val key: IMethod, val coveredLines: Set[Int]) extends MethodCoverageInfo {
	override def toString = key.getName + " : " + coveredLines.toString
}

object EmptyMethodCoverageInfo extends MethodCoverageInfo

object Cache {
	var ccache = Map[IClass, Set[IMethod]]()
	var mcache = Map[IMethod, MethodLineInfo]()
}

case class CoverageInfo(val clsName: String, val coveredLines: List[Int]) {

	def collectMethodCoverageInfos(implicit cha: ClassHierarchy) = {
		val cls = getIClass()

		val ms =
			if (Cache.ccache contains cls)
				Cache.ccache(cls)
			else {
				val temp = cls.getDeclaredMethods filter { m => iS(appCL)(m.getDeclaringClass) }
				Cache.ccache += (cls -> temp.toSet)
				temp
			}

		implicit val cache = new AnalysisCache

		val mis =
			for (m <- ms) yield m match {
				case ibm: IBytecodeMethod =>
					if (Cache.mcache contains ibm)
						Cache.mcache(ibm)
					else {
						//												if (clsName contains "Lorg/apache/commons/cli2/option/ParentTest") {
						//						println(m)
						val mli = MethodLineInfo(ibm)
						//													if (m.toString contains "Application, Lorg/apache/commons/cli/HelpFormatter, rtrim(Ljava/lang/String;)Ljava/lang/String") println(m, mli) 
						Cache.mcache += (m -> mli)
						mli
					}
			}

		mis map { getMCI(_) }
	}

	private def getMCI(mli: MethodLineInfo): MethodCoverageInfo =
		mli match {
			case valid @ ValidMethodLineInfo(key, first, last) => if (valid.isComplete) {
				val covered = Range(first.get, last.get + 1) filter { coveredLines contains _ }
				if (covered.isEmpty) EmptyMethodCoverageInfo
				else {

					//					if (key.toString contains "Lorg/apache/commons/cli/GnuParser, flatten(Lorg/apache/commons/cli/Options") println(key.getSignature, covered.sorted)

					ValidMethodCoverageInfo(key, covered.toSet)
				}
			} else EmptyMethodCoverageInfo
			case _ => EmptyMethodCoverageInfo
		}

	private def getIClass()(implicit cha: ClassHierarchy) =
		cha lookupClass TypeReference.findOrCreate(Types.appCL, TypeName.string2TypeName(clsName))

}

case class ExecFileReader(val clsRoot: File, val execFile: File) {

	def getFile(r: File, clsName: String, t: String) = new File(r, clsName + t)

	def getCoverageBuilder(className: String): CoverageBuilder = {

		val store = new ExecutionDataStore
		val in = new FileInputStream(execFile)
		val reader = new ExecutionDataReader(in)

		reader.setSessionInfoVisitor(new ISessionInfoVisitor() {
			def visitSessionInfo(info: SessionInfo) {}
		})

		reader.setExecutionDataVisitor(new IExecutionDataVisitor() {
			def visitClassExecution(data: ExecutionData) { store.put(data) }
		})

		reader.read()

		val cb = new CoverageBuilder
		val analyzer = new Analyzer(store, cb)

		try {
			val clsFile = new FileInputStream(getFile(clsRoot, className, ".class"))
			analyzer.analyzeClass(clsFile, className)
			clsFile.close()
		} catch {
			//Supposed to be tests
			case e: FileNotFoundException => None //println("Can't find " + clsRoot + "/" + className + ".class")
		}

		in.close()
		cb
	}
}

object ExecFileReader {
	implicit class CoverageBuilderWrapper(cb: CoverageBuilder) {
		def parseBuilder = for (
			cc <- cb.getClasses.toList;
			lines = Range(cc.getFirstLine, cc.getLastLine + 1)
				filter
				//								{ cc.getLine(_).getStatus == ICounter.FULLY_COVERED }
				{
					l =>
						//					if (cc.getName().toString contains "AlreadySelectedException") println("line,"+l+"\t"+cc.getLine(l).getStatus)
						cc.getLine(l).getStatus > ICounter.NOT_COVERED
				}
		) yield new CoverageInfo('L' + cc.getName, lines.toList)
		//			ret foreach {case CoverageInfo(name, lines) => if (name contains "Puzzle") println(name, lines)}
		//			ret
	}
}
