package edu.udel.icdc.mutation

import edu.udel.icdc.main.Config.subjectHome
import com.ibm.wala.ipa.cha.ClassHierarchy
import edu.udel.icdc.coverage.reader.CoverageInfo
import edu.udel.icdc.coverage.reader.ValidMethodCoverageInfo
import com.ibm.wala.classLoader.IMethod

case class MajorInfo(mutants: Seq[RawMutant])

trait RMutant

object FailToParseMutant extends RMutant

case class RawMutant(method: IMethod, line: Int, status: String, covered: Boolean, t: Int) extends RMutant {
	def isCovered = covered
	//	override def toString = idx.toString + " : " + status + "\t"
	override def toString = method + "@" + line + " : " + status + "\t"
	override def equals(other: Any) = other match {
		case that: RawMutant => (method equals that.method) && (this.line == that.line)
		case _ => false
	}
}

object MajorInfo {
	def apply(subject: String)(implicit cha: ClassHierarchy) = {
		val path = subjectHome + subject
		val result = path + "/original/killed.csv"
		val mutants = path + "/mutants.log"

		val parsed = parseCoveredMutants(mutants, result)
		//		parsed foreach {p => print(p.status)}
		new MajorInfo(parsed)
	}

	def parseResult(csv: String) = {
		val r = scala.io.Source.fromFile(csv)
		val lines = r.getLines.toList drop 1
		r.close

		lines map { line => (line.split(",")(0).toInt, line.split(",")(1)) } toMap
	}

	def parseCovered(csv: String) = {
		val f = scala.io.Source.fromFile(csv)
		val lines = f.getLines.toList drop 1
		f.close

		lines map { line => line.split(",")(1).toInt } toSet
	}

	def getLineNo(as: Array[String], index: Int, len: Int): Int = {
		if (index == len) { 0 }
		else
			try {
				as(index).toInt
			} catch {
				case e: Exception => getLineNo(as, index + 1, len)
			}

	}

	def parseCoveredMutants(log: String, r: String)(implicit cha: ClassHierarchy) = {
		val f = scala.io.Source.fromFile(log)
		val ms = f.getLines.toList
		val kinds = List("BIN", "UNR", "AOR", "LOR", "SOR", "COR", "ROR", "LVR", "ORU", "STD")
		f.close

		val result = parseResult(r)
		val covered = result map { case (id, s) => id }

		ms map { m =>
			{
				val splitted = m split ":"

				try {
					splitted(5).toInt

					val clsName = "L" + {
						if (splitted(4) contains "@")
							splitted(4).split("@")(0)
						else
							splitted(4)
					}.replace('.', '/')

					val lines = List(splitted(5).toInt)

					val cinfo = new CoverageInfo(clsName, lines)
					val parsed = cinfo.collectMethodCoverageInfos collect {
						case ValidMethodCoverageInfo(key, coveredLines) => key -> coveredLines
					} toList

					//				if (parsed.length != 1) println(parsed)
					new RawMutant(parsed(0)._1, parsed(0)._2.toList(0),
						result.getOrElse(splitted(0).toInt, "XXX"),
						covered.toSet contains splitted(0).toInt, kinds.indexOf(splitted(1)))
				} catch {
					case _: Exception => FailToParseMutant
				}
			}

		} collect { case m: RawMutant => m } filter { _.isCovered } toSeq
	}

}