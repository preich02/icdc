package edu.udel.icdc.mutation

import java.io.File
import edu.udel.icdc.utils.Types.TestSuiteCoverageInfo
import java.io.FileWriter
import edu.udel.icdc.main.Config
import collection.Map
import com.ibm.wala.types.MethodReference
import edu.udel.icdc.utils.ImplicitConversions.i2ref
import edu.udel.icdc.utils.ImplicitConversions.ref2i

case class RatioRecord(val dc: Int, val ic: Int, val killed: Int) {
	override def toString = {
		ic.toString + "," + dc.toString + "," + killed.toString 
	}
}

class MutationScoreAnalyzer(val dc: Seq[Mutant], val ic: Seq[Mutant]) {
	val icKilledSize = (ic filter { _.killed }).size
	val dcKilledSize = (dc filter { _.killed }).size

	def icScore = icKilledSize.toFloat / ic.size
	def dcScore = dcKilledSize.toFloat / dc.size

	def toCsv(subject: String) = {
		val icWrite = ic map (_.toString + ",1") mkString "\n"
		val dcWrite = dc map (_.toString + ",0") mkString "\n"
		val toWrite = icWrite + "\n" + dcWrite
		val file = new FileWriter(new File(Config.subjectHome + "/" + subject + "/icdc.csv"))
		file.write("killed, freq, ic\n")
		file.write(toWrite)
		file.close()
	}
	
	def perMethod(sub: String) = {
		val all = scala.collection.mutable.Map[MethodReference, Tuple2[Int, Int]]().withDefaultValue((0,0))
		
		dc foreach {mu => all update (mu.m, (all(mu.m)._1+(if (mu.killed) 1 else 0), all(mu.m)._2+1))}
		ic foreach {mu => all update (mu.m, (all(mu.m)._1+(if (mu.killed) 1 else 0), all(mu.m)._2+1))}

		val f = new FileWriter(new File(Config.projectRoot + "/data/ratio/" + sub + ".kill"))
		
		all foreach {case (m, k) => f.write(m.getSignature() + "," + k._1.toFloat/k._2 + "\n")}
		
		f.close()
	}

//	def getMethodRatio(db: TestSuiteCoverageInfo, sub: String) = {
//		val values = db.values
//
//		val rec = scala.collection.mutable.Map[MethodReference, RatioRecord]().withDefaultValue(new RatioRecord(0, 0, 0))
//
//		def update(mu: Mutant, dc: Boolean) = {
//			val r = rec(mu.m)
//			val d = if (dc) 1 else 0
//			val i = if (dc) 0 else 1
//			val k = if (mu.killed) 1 else 0
//			rec update (mu.m, (new RatioRecord(r.dc + d, r.ic + i, r.killed + k)))
//		}
//
//		dc foreach { update(_, true) }
//		ic foreach { update(_, false) }
//
//		val f = new FileWriter(new File(Config.projectRoot + "/data/ratio/" + sub + ".csv"))
//		rec foreach { case (m, r) => f.write(m.getSignature() + "," + r + "\n") }
//		f.close()
//	}

	def updateFrequency(db: TestSuiteCoverageInfo, sub: String) = {
		val values = db.values

		dc foreach { mu => (mu.freq = values count { d => ((d contains mu.m) && (d(mu.m) contains mu.l)) }) }
		ic foreach { mu => (mu.freq = values count { d => ((d contains mu.m) && (d(mu.m) contains mu.l)) }) }

//		val (kdc, sdc) = dc partition (_.killed)
//		val (kic, sic) = ic partition (_.killed)

		//		val top = (sic sortBy {- _.freq}).toList(0)

		//		values foreach {d => if (d contains top.m) {println(top);throw new Exception()}}

//		val f = new FileWriter(new File(Config.projectHome + "/pyscripts/dist/" + sub + ".kind"))
//		f.write(kdc map { _.t } mkString ",")
//		f.write("\n")
//		f.write(sdc map { _.t } mkString ",")
//		f.write("\n")
//		f.write(kic map { _.t } mkString ",")
//		f.write("\n")
//		f.write(sic map { _.t } mkString ",")
//		f.close()

		val ff = new FileWriter(new File(Config.projectRoot + "/data/freq/" + sub + ".csv"))
		dc foreach {mu => ff.write("1,"+(if (mu.killed) "1" else "0")+","+mu.freq.toString+"\n")}
		ic foreach {mu => ff.write("0,"+(if (mu.killed) "1" else "0")+","+mu.freq.toString+"\n")}
		ff.close()
	}

	override def toString =
		"ic:\t" + icScore + "\t(" + icKilledSize + "/" + ic.size + ")\n" +
			"dc:\t" + dcScore + "\t(" + dcKilledSize + "/" + dc.size + ")\n" +
			"overall:\t" + (icKilledSize.toFloat + dcKilledSize) / (ic.size + dc.size)
	//		"ic avg killed freq:\t" + getKilledFreq.sum.toFloat / getKilledFreq.size + "\n" + 
	//		"ic avg live freq:\t\t" + getLiveFreq.sum.toFloat / getLiveFreq.size + "\n\n" + 
	//		"dc avg killed freq:\t" + getDcKilledFreq.sum.toFloat / getDcKilledFreq.size + "\n" + 
	//		"dc avg live freq:\t\t" + getDcLiveFreq.sum.toFloat / getDcLiveFreq.size + "\n\n"

}

case class Mutant(val m: MethodReference, val l: Int, val killed: Boolean, var freq: Int, t: Int) {
	override def toString = "{" + m + l + killed + "," + freq + "}"
}

object MutationScoreAnalyzer {
	def apply(minfo: MajorInfo, dcPool: Map[MethodReference, Set[Int]], icPool: Map[MethodReference, Set[Int]]) = {

		def coveredIn(mut: RawMutant, map: Map[MethodReference, Set[Int]]) = {

			if (mut.status equals "EXC")
				map contains mut.method
			else
				(map contains mut.method) && (map(mut.method) contains mut.line)
		}

		def killed(rm: RawMutant) = rm.status != "LIVE"

		//		val icm = minfo.mutants filter {icoveredIn(_, sic)} map {rm => new Mutant(killed(rm), 0)}
		//		print("missing:\t")
		//		println(minfo.mutants filterNot { m => coveredIn(m, dcPool) || coveredIn(m, icPool) } size)
//				val tofind = "Application, Lorg/apache/commons/cli/GnuParser, flatten(Lorg/apache/commons/cli/Options;[Ljava/lang/String;Z)"
//				val tofind = "AlreadySelectedException"
//				println(dcPool filter {case (k,v) => k.toString contains tofind})
//				println(icPool filter {case (k,v) => k.toString contains tofind})
		//		val dcm = minfo.mutants filter {dcoveredIn(_, sdc)} map {rm => new Mutant(killed(rm), 0)}
		println(dcPool.size, icPool.size)
		val (dd, ii) = minfo.mutants partition { coveredIn(_, dcPool) } //map {rm => new Mutant(killed(rm), 0)}
		val dcm = dd map { rm => new Mutant(rm.method, rm.line, killed(rm), 0, rm.t) }
		val icm = (ii filter { coveredIn(_, icPool) }) map { rm =>
			new Mutant(rm.method, rm.line, killed(rm), 0, rm.t)
		}
		println(dd.size)
		println(icm.size)
		println(dcm.size)

		new MutationScoreAnalyzer(dcm, icm)
	}
}
