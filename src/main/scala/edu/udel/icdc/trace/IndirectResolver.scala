package edu.udel.icdc.trace

import edu.udel.icdc.utils.Types._
import com.ibm.wala.classLoader.IMethod
import scala.collection.mutable.PriorityQueue
import scala.collection.mutable.Map
import edu.udel.icdc.utils.ImplicitConversions.MapWrapper

trait Profile
object EmptyProfile extends Profile

case class TestProfile(test: IMethod, ratio: Double, ds: Map[IMethod, Set[Int]], is: Map[IMethod, Set[Int]])
	extends Ordered[TestProfile] with Profile {
	
	def dSize = (0 /: ds){case (acc, (k,v)) => acc + v.size}

	override def compare(that: TestProfile) =
//		if (ratio == 0.0)
//			this.dSize.compareTo(that.dSize)
//		else
//			this.ratio.compareTo(that.ratio)
		if (ratio == 0.0)
			that.dSize.compareTo(this.dSize)
		else
			that.ratio.compareTo(this.ratio)
			
			
		override def toString = test.getName.toString + "\t" + ratio  //+ "\n\t" + ds + "\n\t" + is + "\n\n"

//	override def toString = ratio.toString // + "\n\t" + ds + "\n\t" + is + "\n\n"

}

case class Analyzer(total: Int, var profileQueue: PriorityQueue[TestProfile]) {
	val addedD = Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)
	val addedI = Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)
	var totalIRatio = 0.0

	private def updateRatio(prev: TestProfile)(tp: TestProfile) = {
		prev.ds foreach {
			case (k, v) => {
				tp.is.update(k, tp.is(k) diff v)
				tp.ds.update(k, v union tp.ds(k))
			}
		}

		val sizeD = tp.ds.cSize
		val sizeI = tp.is.cSize

		val r = if (sizeD == 0 && sizeI == 0) 0.0 else sizeI.toDouble / (sizeI + sizeD)
		//		println(sizeD, sizeI, sizeI.toDouble / (sizeI + sizeD))
		//		if (sizeD==0 && sizeI==0)
		//			EmptyProfile
		//		else
		new TestProfile(tp.test, r, tp.ds, tp.is)
	}

	private def updatedAdded(tp: TestProfile) = {


		tp.ds foreach {
			case (k, v) => 
				addedD.update(k, addedD(k) union v)
				addedI.update(k, addedI(k) diff v)
		}

		tp.is foreach {
			case (k, v) => {
				val ic = v diff addedD(k)
				addedI.update(k, addedI(k) union ic)
			}
		}

		val totI = addedI.cSize
		val totD = addedD.cSize
		totalIRatio = totI.toDouble / (totI + totD)
	}

	def dequeue = {
		val popped = profileQueue dequeue ()
		updatedAdded(popped)
		val f = updateRatio(popped)(_)
		profileQueue = profileQueue map { f(_) } //collect { case tp: TestProfile => tp }
	}
	
	def saturation = (addedI.cSize.toDouble+addedD.cSize)/total
	
	def cInfo = (addedD, addedI)

	def report = {
		val totI = addedI.cSize
		val totD = addedD.cSize
		"directly:\t" + totD + "\tindirectly:\t" + totI + "\tratio\t" + (1 - totalIRatio) + 
				"\tcoverage\t" + saturation
	}
}

object Analyzer {

	def apply(directs: ShallowMap, db: TestSuiteCoverageInfo) = {
		val all = Map[IMethod,Set[Int]]().withDefaultValue(Set.empty)
		
		db foreach {
			case (t, v) =>
				v foreach {
					case (k, vv) => all.update(k, all(k) union vv)
				}
		}

		val totLines = all.cSize
		//_1 direct, _2 indirect
		val splitted = db map { case (t, md) => t -> (md partition { case (m, s) => directs.getOrElse(t, List()).toList contains m }) }

		def getLocalRatio(ds: SingleTestCoverageInfo, is: SingleTestCoverageInfo) = {
			val sizeD = ds.cSize
			val sizeI = is.cSize

			sizeI.toDouble / (sizeI + sizeD)
		}

		val priQueue = new PriorityQueue[TestProfile]()

		splitted foreach {
			case (t, (lds, lis)) =>
				priQueue enqueue new TestProfile(t, getLocalRatio(lds, lis), 
						(Map() ++= lds).withDefaultValue(Set.empty), (Map() ++= lis).withDefaultValue(Set.empty))
		}

		println(totLines)
		new Analyzer(totLines, priQueue)
	}
}