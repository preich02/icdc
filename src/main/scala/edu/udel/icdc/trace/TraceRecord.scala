package edu.udel.icdc.trace

import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.cha.ClassHierarchy

import edu.udel.icdc.utils.ImplicitConversions.SubClassWrapper
import edu.udel.icdc.utils.Types._
import edu.udel.icdc.utils.Types
import edu.udel.icdc.utils.Utils.iS

case class TraceRecord(callee: IMethod, caller: IMethod)(implicit cha: ClassHierarchy) {
//	def isShallow = (caller.getDeclaringClass.getName isSubOf JUnitTest) && (iS(appCL)(callee.getDeclaringClass))
	def isShallow = (callee != null) && (iS(appCL)(callee.getDeclaringClass))
	
}