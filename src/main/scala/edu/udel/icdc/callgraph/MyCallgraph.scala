package edu.udel.icdc.callgraph

import scala.collection.JavaConversions.asScalaIterator
import edu.udel.icdc.main.Config.subjectHome
import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.collection.JavaConversions.seqAsJavaList
import scala.collection.mutable.Queue
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.callgraph.AnalysisCache
import com.ibm.wala.ipa.callgraph.CGNode
import com.ibm.wala.ipa.callgraph.CallGraph
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.util.graph.traverse.DFS
import edu.illinois.wala.Facade.makeFilterFromFunction
import edu.illinois.wala.ipa.callgraph.AnalysisOptions
import edu.illinois.wala.MyCallGraphBuilder
import edu.udel.icdc.utils.Types.appCL
import edu.udel.icdc.utils.Utils
import com.ibm.wala.ipa.callgraph.impl.Util
import com.ibm.wala.types.MethodReference
import com.ibm.wala.util.strings.Atom
import com.ibm.wala.types.Descriptor
import com.ibm.wala.classLoader.Language
import com.ibm.wala.types.TypeReference
import edu.udel.icdc.utils.Types
import edu.illinois.wala.ipa.callgraph.AnalysisScope
import com.ibm.wala.classLoader.ClassLoaderFactoryImpl
import com.ibm.wala.util.scope.JUnitEntryPoints
import scala.util.parsing.json.JSONObject
import scala.io.Source
import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder
import com.ibm.wala.ipa.callgraph.impl.ExplicitCallGraph

case class MyCallgraph(val cg: CallGraph) {

	val sd = buildShallowDeeps
	//	println(sd)

	def directCalls: Map[IMethod, Iterable[IMethod]] =
		sd map { e => e._1 -> e._2._1 } toMap
	//		cg.getEntrypointNodes map { entry => entry.getMethod -> getShallows(entry) } toMap

	def indirectCalls: Map[IMethod, Iterable[IMethod]] =
		sd map { e => e._1 -> e._2._2 } toMap
	//		cg.getEntrypointNodes map { entry => entry.getMethod -> getDeeps(entry) } toMap

	private def isApplicationNode(n: CGNode) =
		n.getMethod.getDeclaringClass.getClassLoader.getReference equals appCL

	private def getShallows(entry: CGNode) =
		cg.getSuccNodes(entry).toList filter { n => isApplicationNode(n) } map { _.getMethod }
	//		cg.getSuccNodes(entry).toList map { _.getMethod }

	private def getDeeps(entry: CGNode) =
		DFS.getReachableNodes(
			cg, cg.getSuccNodes(entry).toList, { n: CGNode =>
				isApplicationNode(n) && n.getMethod.isPublic
			}) map { _.getMethod }

	private def buildShallowDeeps =
		cg.getEntrypointNodes map { e => e.getMethod -> getShallowDeeps(e) } toMap
	//		filter { n =>
	//			n.getMethod.toString contains "Teststack4, testEmptyPush()V"
	//			n.getMethod.toString contains "testGetReservedWord()V"
	//		} 

	private def getShallowDeeps(entry: CGNode) = {
		val testClass = entry.getMethod.getDeclaringClass
		//		println("entry: ", entry.getMethod)

		val Q = new Queue[CGNode]()
		var S = Set[IMethod]()
		var D = Set[IMethod]()

		Q enqueue entry

		var depth = 0

		while (Q.nonEmpty && depth < 10) {
			//			println(Q map { _.getMethod.getName.toString })
			val last = Q.dequeue
			//			println(cg.getSuccNodes(last).toList map { _.getMethod.toString })
			val ss = cg.getSuccNodes(last).toList

			if ((last.getMethod.getDeclaringClass equals testClass) ||
				(last.getMethod.getDeclaringClass.getReference equals Types.JUnitTest))
				(ss filter isApplicationNode) foreach { m =>
					//					println("shallow: ", indent, "\t", m.getMethod.getSignature, "<-?", last.getMethod.getSignature);
					S += m.getMethod
				}
			else
				(ss filter isApplicationNode) foreach { m =>
					//					println("deep: ", indent, "\t", m.getMethod.getSignature);
					D += m.getMethod
				}

			ss foreach { Q enqueue _ }
			depth += 1
		}

		(S, D)
	}
}

object MyCallgraph {
	def apply(subject: String)(implicit cha: ClassHierarchy, opts: AnalysisOptions) = {

		//		val jsonFile = subjectHome + subject + "/cg.json"
		//		//		val tests = Utils.getTestMethods(subject)
		//		if (Files.exists(Paths.get(jsonFile))) {
		//			val r = scala.io.Source.fromFile(jsonFile)
		//			val json = r.getLines.mkString
		//			r.close
		//
		//			val cg = JacksMapper.readValue[ExplicitCallGraph](json)
		//
		//			println("size of cg:")
		//			println(cg.getNumberOfNodes())
		//
		//			new MyCallgraph(cg)
		//		} else {
		//			val entryPoints = JUnitEntryPoints.make(cha)
		//			val newOptions = new AnalysisOptions(opts.scope, entryPoints, cha, false, null, null)
		//			//		newOptions.setMaxNumberOfNodes(100000)
		//
		//			val cache = new AnalysisCache
		//			//		val builder = Util.makeRTABuilder(newOptions, cache, cha, opts.scope)
		//			val builder = Util.makeZeroCFABuilder(newOptions, cache, cha, opts.scope)
		//			val cg = builder.makeCallGraph(newOptions, null)
		//
		//			println("caching cg to json file...")
		//			val json = JacksMapper.writeValueAsString[ExplicitCallGraph](cg.asInstanceOf[ExplicitCallGraph])
		//			val output = new PrintWriter(new File(subjectHome + subject + "/cg.json"))
		//			output.write(json)
		//			output.close
		//
		//			println("size of cg:")
		//			println(cg.getNumberOfNodes())
		//
		//			new MyCallgraph(cg)
		//		}
		val size = JUnitEntryPoints.make(cha).iterator.toList.size
		val entryPoints = JUnitEntryPoints.make(cha)
		val newOptions = new AnalysisOptions(opts.scope, entryPoints, cha, false, null, null)
		//		newOptions.setMaxNumberOfNodes(100000)
		println("tests : ", size)

		val cache = new AnalysisCache
		//		val builder = Util.makeRTABuilder(newOptions, cache, cha, opts.scope)
		val builder = Util.makeZeroCFABuilder(newOptions, cache, cha, opts.scope)
		val cg = builder.makeCallGraph(newOptions, null)

		println("size of cg:")
		println(cg.getNumberOfNodes())

		new MyCallgraph(cg)
		//		new MyCallgraph(MyCallGraphBuilder(newOptions).cg)
	}
}
