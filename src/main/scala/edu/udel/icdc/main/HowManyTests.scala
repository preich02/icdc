package edu.udel.icdc.main

import edu.udel.icdc.utils.Utils

object HowManyTests {
	def main(args: Array[String]) = {
		val subject = args(0)
		val opts = Utils.getOptions(subject + ".conf")
		
		implicit val cha = opts.cha
		println(Utils.getTestMethods(subject).length)
	}
}
