package edu.udel.icdc.main

import edu.udel.icdc.coverage.analyzer.BasicAnalyzer
import edu.udel.icdc.mutation.MajorInfo
import edu.udel.icdc.mutation.MutationScoreAnalyzer
import edu.udel.icdc.main.Config.subjectHome

object NoArgMutationAnalysis {
	def main(args: Array[String]) = {
		val subject = "commons-cli2"
//		val subject = args(0)
		val sa = SubjectRunner(subject)
		implicit val ch = sa.cha
		implicit val opt = sa.opts
		


		println("reading coverage info...")
		val db = sa.readOneSubject
		
//		db foreach {case (t, s) => s foreach {case (m, ss) => if (m.toString contains "Application, Lorg/apache/commons/cli/HelpFormatter, rtrim(Ljava/lang/String;)Ljava/lang/String") println(m,ss)}}
//		db foreach {case (t, s) => if (t.toString contains "Application, Lorg/apache/commons/cli/ParserTestCase, testDoubleDash()V") println(t,s)}

		println("reading trace files...")
		val directs = sa.readTraceFiles
		

		val ba = BasicAnalyzer(directs, db)
		ba.analyze
		
//		ba.writeRatio(subject)
//		ba.study(subject)
		ba.overloading(subject)
		ba.inheritance(subject)

		//		val (dcPool, icPool) = analyzer.cInfo
		//		
//		println("gathering major info...")
//		val minfo = MajorInfo(subject)
//		println("analyze mutation info...")
//		val majorAnalyzer = MutationScoreAnalyzer(minfo, ba.getDC, ba.getIC)
//
//		majorAnalyzer.updateFrequency(db, subject)
//		majorAnalyzer.perMethod(subject)
		//		
//		println(majorAnalyzer)
	}
}