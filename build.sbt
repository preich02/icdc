// set the name of the project
name := "icdc"

version := "0.1"

organization := "University of Delaware"

scalaVersion := "2.10.2"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

unmanagedJars in Compile ~= {uj => 
    Seq(Attributed.blank(file(System.getProperty("java.home").dropRight(3)+"lib/tools.jar"))) ++ uj
	}

libraryDependencies ++= Seq(
	"junit" % "junit" % "4.+",
	"com.typesafe" % "config" % "0.5.+",
	"org.jacoco" % "org.jacoco.core" % "0.7.2.201409121644",
	"org.jacoco" % "org.jacoco.agent" % "0.7.2.201409121644",
	//"org.jacoco" % "org.jacoco.agent.rt" % "0.7.2.201409121644",
	"org.jacoco" % "org.jacoco.report" % "0.7.2.201409121644",
	"com.lambdaworks" %% "jacks" % "2.3.3",
	//"org.jacoco" % "jacoco-maven-plugin" % "0.7.2.201409121644",
	"com.ibm.wala" % "com.ibm.wala.shrike" % "1.5.3",
	"com.ibm.wala" % "com.ibm.wala.util" % "1.5.3",
	"com.ibm.wala" % "com.ibm.wala.cast" % "1.5.3",
	"com.ibm.wala" % "com.ibm.wala.core" % "1.5.3")
